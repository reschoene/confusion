#!/bin/bash

echo ======== DEPLOY Confusion : INÍCIO ========

echo compactando arquivos de recursos html, css, js...
echo 
rm ./confusion.tar.bz2
tar -cvjf confusion.tar.bz2 ./*
echo 
echo fim da compactacao dos arquivos de recursos

echo Subindo confusion.tar.bz2...
echo 
sshpass -vp zz4115 scp /home/renato/Coursera/Bootstrap4/conFusion/confusion.tar.bz2 root@vps6174.publiccloud.com.br:/home/renato/confusion.tar.bz2
echo 
echo Fim upload confusion.tar.bz2

echo Conectando ao servidor da Locaweb...
sshpass -p zz4115 ssh -o StrictHostKeyChecking=no root@vps6174.publiccloud.com.br << EOF_OF_COMMAND
  echo Parando serviço nginx
  service nginx stop

  echo excluindo versao antiga
  cd /usr/share/nginx/html
  rm -R confusion

  echo preparando arquivo para descompactacao
  cd  /home/renato
  rm -R confusion
  mkdir confusion
  mv confusion.tar.bz2 ./confusion
  cd confusion

  echo descompactando arquivo confusion.tar.bz2
  tar -xvjf confusion.tar.bz2

  echo excluindo arquivo compactado
  rm ./confusion.tar.bz2

  echo movendo arquivo descompactado para o diretorio do servidor nginx
  mv /home/renato/confusion /usr/share/nginx/html/confusion

  echo Iniciar serviço nginx
  service nginx start
EOF_OF_COMMAND

echo ======== DEPLOY Confusion : FIM ========